/*Ajax подгрузка контента в бекофис*/
function showContent(link, id) {
    jQuery(".menu-btn").css("background-color", "#34495e")
    jQuery(".menu-btn").css("background-color", "inherit")
    jQuery(id).css("background-color", "rgba(0, 0, 0, 0.3)")


    var cont = document.getElementById('content');
    var loading = document.getElementById('loading');
    cont.innerHTML = loading.innerHTML;
    var http = createRequestObject();

    if (http) {
        http.open('get', link);
        http.onreadystatechange = function () {
            if (http.readyState == 4) {
                cont.innerHTML = http.responseText;
            }
        }
        http.send(null);
    }
    else {
        document.location = link;
    }
}
// ajax объект
function createRequestObject() {
    try {
        return new XMLHttpRequest()
    }
    catch (e) {
        try {
            return new ActiveXObject('Msxml2.XMLHTTP')
        }
        catch (e) {
            try {
                return new ActiveXObject('Microsoft.XMLHTTP')
            }
            catch (e) {
                return null;
            }
        }
    }
}
/* Конец Ajax подгрузки контента в бекофис*/

/*Всплывающее окно регистрации и входа*/
jQuery(document).ready(function () {
    //Скрыть PopUp при загрузке страницы    
    PopUpRegHide()
    PopUpLoginHide()
    PopUpSettingsHide()
    PopUpSignalsHide()
    PopUpStatsHide()

    jQuery(".tabs").lightTabs();


});
//Функция отображения регистрации
function PopUpRegShow() {
    jQuery("#popup1").show();
}
//Функция скрытия регистрации
function PopUpRegHide() {
    jQuery("#popup1").hide();
}
//Функция отображения входа
function PopUpLoginShow() {
    jQuery("#popup2").show();
}
//Функция скрытия входа
function PopUpLoginHide() {
    jQuery("#popup2").hide();
}
//Функция всплывающего окна настроек
function PopUpSettingsShow() {
    jQuery("#popup3").show();
}
//Функция всплывающего окна настроек
function PopUpSettingsHide() {
    jQuery("#popup3").hide();
}
//Функция всплывающего окна настроек
function PopUpSignalsShow() {
    jQuery("#popup4").show();
}
//Функция всплывающего окна настроек
function PopUpSignalsHide() {
    jQuery("#popup4").hide();
}
//Функция всплывающего окна статистики
function PopUpStatsShow() {
    jQuery("#popup5").show();
}
function PopUpStatsHide() {
    jQuery("#popup5").hide();
}
//Функция перехода на регистрацию
function PopUpRegister() {
    jQuery("#popup2").hide();
    jQuery("#popup1").show();
}

function enableBtn() {
    document.getElementById("register").disabled = false;
}

/*Конец всплывающего окна регистрации*/
/*Вкладки jquery */

(function ($) {
    jQuery.fn.lightTabs = function (options) {

        var createTabs = function () {
            tabs = this;
            i = 0;

            showPage = function (i) {
                jQuery(tabs).children("div").children("div").hide();
                jQuery(tabs).children("div").children("div").eq(i).show();
                jQuery(tabs).children("ul").children("li").removeClass("active");
                jQuery(tabs).children("ul").children("li").eq(i).addClass("active");
            }

            showPage(0);

            jQuery(tabs).children("ul").children("li").each(function (index, element) {
                jQuery(element).attr("data-page", i);
                i++;
            });

            jQuery(tabs).children("ul").children("li").click(function () {
                showPage(parseInt(jQuery(this).attr("data-page")));
            });
        };
        return this.each(createTabs);
    };
})(jQuery);


jQuery(function () {
    jQuery("#connectBroker").on('click', function (e) {
        jQuery("#popup1").show();

        e.preventDefault(e);
        return false;
    });

    function show() {
        jQuery.ajax({
            url: "/site/invest",
            cache: false,
            success: function (html) {
                jQuery(".table-invest").html(html);
            }
        });
    }

    show();
    setInterval(show, 10000);


    function show2() {
        jQuery.ajax({
            url: "/site/static",
            cache: false,
            success: function (html) {
                jQuery(".table-static").html(html);
            }
        });
    }

    show2();
});


