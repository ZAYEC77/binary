vendors-update:
	composer update
vendors-install:
	composer install -o
db-migrate:
	php yii core/migrate
message:
	php yii message messages/config.php