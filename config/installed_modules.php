<?php
return [
    "admin" => [
        "class" => "nullref\\fulladmin\\Module"
    ],
    "user" => [
        "class" => "nullref\\fulladmin\\modules\\user\\Module",
        "controllerMap" => [
            "admin" => "nullref\\fulladmin\\modules\\user\\controllers\\AdminController",
            "registration" => [
                "class" => "dektrium\\user\\controllers\\RegistrationController",
                "viewPath" => "@dektrium/user/views/registration"
            ]
        ]
    ]
];