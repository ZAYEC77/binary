<?php

namespace app\migrations;

use yii\db\Migration;

class m161024_221609_add_user_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'telephone', $this->string());
        $this->addColumn('{{%user}}', 'broker_id', $this->integer());
        $this->addColumn('{{%user}}', 'country_id', $this->integer());
        $this->addColumn('{{%user}}', 'name', $this->string());
        $this->addColumn('{{%user}}', 'surname', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'telephone');
        $this->dropColumn('{{%user}}', 'broker_id');
        $this->dropColumn('{{%user}}', 'country_id');
        $this->dropColumn('{{%user}}', 'name');
        $this->dropColumn('{{%user}}', 'surname');
    }
}
