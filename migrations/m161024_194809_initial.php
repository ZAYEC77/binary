<?php

namespace app\migrations;

use yii\db\Migration;
use yii\helpers\Console;

class m161024_194809_initial extends Migration
{

    public function up()
    {
        $query = file_get_contents(\Yii::getAlias('@app/migrations/data/schema_n_data.mysql.sql'));
        $queries = explode(';', $query);
        foreach ($queries as $item) {
            try {
                $this->execute((string)$item);
            } catch (\Exception $e) {
                Console::output($e->getMessage());
            }
        }
    }

    public function down()
    {
        $this->dropTable('invest');
        $this->dropTable('signal_static');
    }

}
