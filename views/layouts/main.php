<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="b-container">
            <div class="wrapper ">

                <div class="row header">

                    <div class="col-md-9 mainheader">
                        <div class="row">
                            <div class="col-md-3 logo"><img src="/img/logo3.png" alt=""></div>

                            <!-- Разделы меню -->
                            <div class="col-md-9">

                                <nav id="main-menu">
                                    <ul class="nav-bar">
                                        <li class="nav-button-rate"><a href="#"></a></li>

                                        <li class="nav-button-stats"><a href="javascript:PopUpStatsShow()"></a></li>
                                        <li class="nav-button-time"><a href="#"></a></li>
                                        <li class="nav-button-manage"><a href="#"></a></li>
                                        <li class="nav-button-settings"><a href="javascript:PopUpSettingsShow()"></a>
                                        </li>
                                        <li class="nav-button-sensor"><a href="javascript:PopUpSignalsShow()"></a></li>
                                    </ul>
                                </nav>

                            </div>
                            <!-- Конец разделов меню -->

                        </div>
                    </div>

                    <!-- Кнопки входа -->
                    <div class="col-md-3 rightheader">
                        <div class="row">
                            <div class="col-md-10 broker" id="connectBroker">
                                <h1>Подключить брокера</h1>
                            </div>
                            <div class="col-md-2 login"><a href="javascript:PopUpLoginShow()"><h1></h1></a></div>
                        </div>
                    </div>
                    <!-- Конец кнопок меню -->

                </div>

                <div class="bodyer">
                    <?= $content ?>
                </div>
                <!--Начало футера-->
                <div class="row">
                    <div class="bottom"></div>
                </div>
            </div>
            <!--Конец футера-->
        </div>
    </div>

    <?= $this->render('_modal') ?>

    <div class="col-md-1"></div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
