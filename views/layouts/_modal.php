<?php
/**
 *
 */
use app\widgets\RegisterForm;

?>

<!-- Форма регистрации -->
<div class="b-popup1 modal-hide" id="popup1">
    <div class="row b-popup-content1">

        <div class="top">
            <h1>Регистрация</h1><span class="success">TEST</span>
            <a href="javascript:PopUpRegHide()" class="clear"><img src="/img/clear.png" alt=""></a>
        </div>

        <div class="form-field">
            <?= RegisterForm::widget() ?>
        </div>

    </div>
</div>
<!-- Конец формы регистрации -->

<!--Начало формы входа-->
<div class="b-popup2 modal-hide" id="popup2">
    <div class="row b-popup-content2">

        <div class="row login-form">
            <div class="col-md-4"></div>
            <div class="col-md-4 form-bg">
                <div class="login-header">
                    <h2>Личный кабинет</h2>
                    <a href="javascript:PopUpLoginHide()" class="clearlogin"><img src="/img/clear.png" alt=""></a>

                </div>
                <div class="logologin">
                    <img src="/img/logologin.png" alt="">
                </div>


                <form action="login.php" method="post" name="login">
                    <div class="row">

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="formslogin">
                                <!--****  testreg.php - это адрес обработчика. То есть, после нажатия на кнопку  "Войти", данные из полей отправятся на страничку testreg.php методом  "post" ***** -->

                                <input name="mail" type="text" class="emaillogin" placeholder="Ввести почту">
                                <input name="pass" type="text" class="passwordlogin" placeholder="Ввести пароль">
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                    </div>
                    <div class="ipcheck">
                        <input type="checkbox" name="not_attach_ip">
                        Не прикреплять к IP (не безопасно) <br>
                        <!--**** Кнопочка (type="submit") отправляет данные на страничку testreg.php ***** -->
                    </div>

                    <button type="submit" name="submit" class="btn btn-login">Войти</button>


                </form>

                <p>Не зарегистрированы? <a href="javascript:PopUpRegister()"><u>Подключите брокера</u></a></p>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>

<!--Конец формы входа-->

<!--            Форма настроек-->
<div class="b-popup3 modal-hide" id="popup3">
    <div class="row b-popup-content3">

        <div class="header">
            <h2>Настройки</h2><a href="javascript:PopUpSettingsHide()" class="clearsettings"><img
                    src="/img/clear.png" alt=""></a>
        </div>
        <div class="settingscontent">
            <div class="row">
                <div class="col-md-9">
                    <div class="setting1"><p>Привлечение сигналов</p></div>
                    <div class="setting2"><p>Включить звук</p></div>
                    <div class="setting3"><p>Автоторговля</p></div>
                    <div class="setting4"><p>Сделка в 1 клик</p></div>
                    <div class="setting5"><p>Скрыть окно результатов сделки</p></div>
                </div>
                <div class="col-md-3 checkboxes">
                    <label class='input1'><input type="checkbox" class="ios-switch"/>
                        <div>
                            <div></div>
                        </div>
                    </label>
                    <label class='input2'><input type="checkbox" class="ios-switch"/>
                        <div>
                            <div></div>
                        </div>
                    </label>
                    <label class='input3'><input type="checkbox" class="ios-switch"/>
                        <div>
                            <div></div>
                        </div>
                    </label>
                    <label class='input4'><input type="checkbox" class="ios-switch"/>
                        <div>
                            <div></div>
                        </div>
                    </label>
                    <label class='input5'><input type="checkbox" class="ios-switch"/>
                        <div>
                            <div></div>
                        </div>
                    </label>

                </div>
            </div>
            <button class="btn save"><a href="javascript:PopUpSettingsHide()">Сохранить</a></button>
        </div>

        <!--                    <label><input type="checkbox" class="ios-switch green" /><div><div></div></div></label>-->
    </div>
</div>
<!--Конец формы настроек-->


<!--Форма сигналов-->
<div class="b-popup4 modal-hide" id="popup4">
    <div class="row b-popup-content4">
        <div class="header">
            <h2>Сигналы</h2>
            <a href="javascript:PopUpSignalsHide()" class="clearlogin"><img src="/img/clear.png" alt=""></a>
        </div>
        <div class="row">

            <div class="col-md-1"></div>
            <div class="col-md-10 table-invest">
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>

<!--Конец формы сигналов-->
<!--Начало формы статистики-->
<div class="b-popup5 modal-hide" id="popup5">
    <div class="row b-popup-content5">
        <div class="header">
            <h2>Статистика сигналов</h2>
            <a href="javascript:PopUpStatsHide()" class="clearlogin"><img src="/img/clear.png" alt=""></a>
        </div>
        <div class="row">

            <div class="col-md-1"></div>
            <div class="col-md-10 table-static">
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
<!--Конец формы статистики-->
