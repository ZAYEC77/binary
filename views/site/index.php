<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="row">
    <div class="col-md-9 chart">

        <!— TradingView Widget BEGIN —>
        <script type="text/javascript" src="https://d33t3vvu2t2yu5.cloudfront.net/tv.js"></script>
        <script type="text/javascript">
            new TradingView.widget({
                "autosize": true,
                "symbol": "EURUSD",
                "interval": "1",
                "timezone": "Etc/UTC",
                "theme": "White",
                "style": "1",
                "locale": "ru",
                "toolbar_bg": "#f1f3f6",
                "enable_publishing": false,
                "withdateranges": true,
                "hide_side_toolbar": false,
                "allow_symbol_change": true,
                "hideideas": true
            });
        </script>
        <!— TradingView Widget END —>


    </div>

    <!--Начало правого блока с сигналами-->
    <div class="col-md-3 api_broker">
        <div class="tabs">

            <ul>
                <li><img src="/img/cube.png" alt="">Торговля</li>
                <li><img src="/img/signals.png" alt="">Сигналы</li>

            </ul>

            <div>
                <div class='content'>
                    <div class="row">
                        <div class="col-md-1 col-lg-1"></div>
                        <div class="col-md-10 col-lg-10">
                            <div class="row">
                                <label for="">Брокер:<br><select name="" id="" class="form-control">
                                        <option>Broker1</option>
                                        <option>Broker1</option>
                                        <option>Broker1</option>
                                        <option>Broker1</option>
                                    </select></label>
                            </div>
                            <div class="row">
                                <label for="">Актив:<br><select name="" id="" class="form-control">
                                        <option>Актив1</option>
                                        <option>Актив1</option>
                                        <option>Актив1</option>
                                        <option>Актив1</option>
                                    </select></label>
                            </div>
                            <div class="row">
                                <label for="">Тип опциона:
                                    <ul class='optiontype'>
                                        <li>Турбо</li>
                                        <li>Класика</li>
                                    </ul>
                                </label>
                            </div>
                            <div class="row">
                                <label for="">Истечение (в секундах):
                                    <ul class='sec'>
                                        <li>30</li>
                                        <li>60</li>
                                        <li>120</li>
                                        <li>180</li>
                                        <li>300</li>
                                    </ul>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="blok1"><label for="">Стоимость:<br> <input
                                                type="text">
                                            <br>$5-$250</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="blok2">Прибыль:(от 70%):<br>$0<br>Страховка:<br>$0
                                    </div>
                                </div>

                            </div>

                            <div class="row btns">
                                <div class="col-md-6">
                                    <button class="btn call"><img src="/img/strelkadown.png" alt="">Ниже
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn put"><img src="/img/strelkaup.png" alt="">Выше
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div>
                                    <label for="" class='checkbox'><input type="checkbox">Сделка в
                                        один клик</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1"></div>
                    </div>


                </div>
                <!--Сигналы-->
                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam quae natus
                    modi quidem impedit in dolore repellat corporis dignissimos. Ipsum minus
                    debitis, consequatur aut pariatur nemo suscipit voluptatibus. Dignissimos,
                    voluptatum.
                </div>
            </div>
        </div>
    </div>

    <!--Конец правого блока с сигналами-->

</div>
