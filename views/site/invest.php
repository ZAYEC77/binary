<?php
/**
 *
 */
use app\models\Invest;

?>


<?php
//Условие на день недели
$today = date("w");

if ($today > 0 && $today < 6) :
    function textimg($par)
    {
        $up = "<img src='http://4.4.ignis-g.com/img/up.png'>";
        $down = "<img src='http://4.4.ignis-g.com/img/down.png'>";
        $dash = "<img src='http://4.4.ignis-g.com/img/dash.png'>";
        switch ($par) {
            case 'Продавать':
                $parimg = $down;
                echo '<td>' . rand(75, 87) . '%' . $parimg . '</td>';
                break;
            case 'Активно продавать':
                $parimg = $down;
                echo '<td>' . rand(88, 97) . '%' . $parimg . '</td>';
                break;
            case 'Нейтрально':
                $parimg = $dash;
                echo '<td>' . $parimg . '</td>';
                break;
            case 'Покупать':
                $parimg = $up;
                echo '<td>' . rand(75, 87) . '%' . $parimg . '</td>';
                break;
            case 'Активно покупать':
                $parimg = $up;
                echo '<td>' . rand(88, 97) . '%' . $parimg . '</td>';
                break;
        }
    }

    ?>


    <table border="1">
        <thead>
        <tr>
            <th>Актив</th>
            <th>Цена покупки</th>
            <th>Время покупки ( GMT+0 )</th>
            <th>5 мин</th>
            <th>15 мин</th>
            <th>1 час</th>
            <th>1 день</th>
            <th>1 месяц</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $batch = Invest::find()->limit(15)->asArray()->orderBy(['time' => SORT_DESC])->each();


        // выводим в HTML-таблицу все данные клиентов из таблицы MySQL

        foreach ($batch as $data) {
            echo '<tr>';
            switch ($data['activ']) {
                case 'eur-usd':
                    $activ = 'EUR/USD';
                    break;
                case 'eur-rub':
                    $activ = 'EUR/RUB';
                    break;
                case 'usd-rub':
                    $activ = 'USD/RUB';
                    break;
                case 'gbp-usd':
                    $activ = 'GBP/USD ';
                    break;
                case 'usd-jpy':
                    $activ = 'USD/JPY ';
                    break;
                case 'eur-jpy':
                    $activ = 'EUR/JPY ';
                    break;
                case 'aud-usd':
                    $activ = 'AUD/USD';
                    break;
                case 'aud-cad':
                    $activ = 'AUD/CAD';
                    break;
                case 'gbp-jpy':
                    $activ = 'GBP/JPY';
                    break;
                case 'gbp-cad':
                    $activ = 'GBP/CAD';
                    break;
                case 'nzd-usd':
                    $activ = 'NZD/USD';
                    break;
                case 'usd-cad':
                    $activ = 'USD/CAD';
                    break;
                case 'usd-chf':
                    $activ = 'USD/CHF';
                    break;
                case 'gold':
                    $activ = 'GOLD';
                    break;
                case 'brent-oil':
                    $activ = 'Brent OIL';
                    break;
                default:
                    $activ = '';
            }
            echo '<td class="activ">' . $activ . '</td>';
            echo '<td>' . $data['price'] . '</td>';
            echo '<td>' . $data['time'] . '</td>';
            //использование функции для записи в бд
            textimg($data['five_value']);
            textimg($data['fifteen_value']);
            textimg($data['hour']);
            textimg($data['day']);
            textimg($data['month']);
            echo '</tr>';
        }

        ?>
        </tbody>
    </table>
<?php else: ?>
    <span>В данный момент рынок не работает. Рынок откроется в понедельник 00:00 ( GMT +0 )</span>

<?php endif ?>

