<?php
/**
 *
 */
use app\models\SignalStatic;

?>
<?php


//прописываем картинки

//функция которая выводит картинки вместо текста
function signal_img($par)
{
    $up = "<img src='http://4.4.ignis-g.com/img/up.png'>";
    $down = "<img src='http://4.4.ignis-g.com/img/down.png'>";
    $dash = "<img src='http://4.4.ignis-g.com/img/dash.png'>";
    switch ($par) {
        case 'Down':
            $parimg = $down;
            echo '<td>' . $parimg . '</td>';
            break;
        case 'Neutral':
            $parimg = $up;
            echo '<td>' . $parimg . '</td>';
            break;
        case 'Up':
            $parimg = $up;
            echo '<td>' . $parimg . '</td>';
            break;
    }
}

function deal_img($par)
{
    $ok = "<img src='http://4.4.ignis-g.com/img/ok.png'>";
    $notok = "<img src='http://4.4.ignis-g.com/img/notok.png'>";
    $dash = "<img src='http://4.4.ignis-g.com/img/dash.png'>";
    switch ($par) {
        case 'success':
            $parimg = $ok;
            echo '<td>' . $parimg . '</td>';
            break;
        case 'neutral':
            $parimg = $dash;
            echo '<td>' . $parimg . '</td>';
            break;
        case 'unsuccess':
            $parimg = $notok;
            echo '<td>' . $parimg . '</td>';
            break;
    }
}


$bach = SignalStatic::find()->each();

// выводим на страницу сайта заголовки HTML-таблицы
echo '<table border="1">';
echo '<thead>';
echo '<tr>';
echo '<th>Актив</th>';
echo '<th>Сигнал</th>';
echo '<th>Время открытия ( GMT+0 )</th>';
echo '<th>Время закрытия ( GMT+0 )</th>';
echo '<th>Цена открытия</th>';
echo '<th>Цена закрытия</th>';
echo '<th>Результат сделки</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';


// выводим в HTML-таблицу все данные клиентов из таблицы MySQL
foreach ($bach as $data) {
    echo '<tr>';
    echo '<td>' . $data['activ'] . '</td>';
    signal_img($data['activ_signal']);
    echo '<td>' . $data['opentime'] . '</td>';
    echo '<td>' . $data['closetime'] . '</td>';
    echo '<td>' . $data['openprice'] . '</td>';
    echo '<td>' . $data['closeprice'] . '</td>';
    deal_img($data['rezdeal']);
    echo '</tr>';
}

echo '</tbody>';
echo '</table>';

?>
