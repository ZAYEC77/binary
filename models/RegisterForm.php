<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\models;


use nullref\fulladmin\modules\user\models\User;
use Yii;
use yii\base\Model;
use yii\validators\CompareValidator;

class RegisterForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $countryId;
    public $telephone;
    public $brokerId;
    public $password;
    public $passwordConfirm;
    public $confirm;

    public function init()
    {
        parent::init();

        $this->name = 'Їжак';
        $this->surname = 'Їжак';
        $this->email = 'test@test.com';
        $this->password = 1;
        $this->passwordConfirm = 1;
    }


    public function rules()
    {

        $user = Yii::$app->getModule('user')->modelMap['User'];
        return [
            [['name', 'surname', 'email', 'countryId', 'telephone', 'password', 'passwordConfirm', 'confirm'], 'trim'],
            [['name', 'surname', 'email', 'countryId', 'telephone', 'brokerId', 'password', 'passwordConfirm', 'confirm'], 'required'],
            ['email', 'email'],
            ['passwordConfirm', CompareValidator::className(), 'compareAttribute' => 'password',
                'message' => Yii::t('user', 'Password are not equal')],
            ['telephone', 'match', 'pattern' => '/\+[0-9]{9,16}/'],
            [['name', 'surname'], 'match', 'pattern' => '/[a-zA-Zа-яА-ЯїЇёЁґҐъЪіІ]{1,}/'],
            ['confirm', 'boolean'],
            ['confirm', 'compare', 'compareValue' => true, 'message' => Yii::t('app', 'You have to confirm')],
            ['password', 'string', 'min' => 6, 'max' => 72],
            'emailUnique' => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This email address has already been taken')
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function register()
    {
        if (!$this->validate()) {
            return false;
        }
        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $user->password = $this->password;
        $user->setAttributes([
            'username' => $this->email,
            'email' => $this->email,
            'password' => $this->password,
            'country_id' => $this->countryId,
            'broker_id' => $this->brokerId,
            'name' => $this->name,
            'surname' => $this->surname,
        ], false);

        if (!$user->register()) {
            $this->addErrors($user->getErrors());
            return false;
        }
        return true;
    }
}