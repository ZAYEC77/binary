<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invest".
 *
 * @property integer $id
 * @property string $activ
 * @property double $price
 * @property string $time
 * @property string $five_value
 * @property string $fifteen_value
 * @property string $hour
 * @property string $day
 * @property string $month
 */
class Invest extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activ', 'price', 'time', 'five_value', 'fifteen_value', 'hour', 'day', 'month'], 'required'],
            [['price'], 'number'],
            [['time'], 'safe'],
            [['activ'], 'string', 'max' => 11],
            [['five_value', 'fifteen_value', 'hour', 'day', 'month'], 'string', 'max' => 24],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activ' => 'Activ',
            'price' => 'Price',
            'time' => 'Time',
            'five_value' => 'Five Value',
            'fifteen_value' => 'Fifteen Value',
            'hour' => 'Hour',
            'day' => 'Day',
            'month' => 'Month',
        ];
    }
}
