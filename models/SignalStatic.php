<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "signal_static".
 *
 * @property integer $id
 * @property string $activ
 * @property string $activ_signal
 * @property string $opentime
 * @property string $closetime
 * @property double $openprice
 * @property double $closeprice
 * @property double $rezprice
 * @property string $rezdeal
 */
class SignalStatic extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'signal_static';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activ', 'activ_signal', 'opentime', 'closetime', 'openprice', 'closeprice', 'rezprice', 'rezdeal'], 'required'],
            [['opentime', 'closetime'], 'safe'],
            [['openprice', 'closeprice', 'rezprice'], 'number'],
            [['activ', 'activ_signal', 'rezdeal'], 'string', 'max' => 14],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activ' => 'Activ',
            'activ_signal' => 'Activ Signal',
            'opentime' => 'Opentime',
            'closetime' => 'Closetime',
            'openprice' => 'Openprice',
            'closeprice' => 'Closeprice',
            'rezprice' => 'Rezprice',
            'rezdeal' => 'Rezdeal',
        ];
    }
}
