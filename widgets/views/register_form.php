<?php
/**
 *
 * @var \app\models\RegisterForm $model
 * @var \yii\web\View $this
 */
use app\helpers\Brokers;
use app\helpers\Geo;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJs(<<<JS
var registerForm = jQuery('#registerForm');
registerForm.on('ajaxComplete', function(e) {
  var isValidate = registerForm.yiiActiveForm('data').validated;
  if (isValidate){
      alert('Ok');
      window.location = '/site/index';
  }
});

JS
);

?>


<?php $form = ActiveForm::begin([
    'id' => 'registerForm',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'action' => '/site/register',
]) ?>

<div class="row">
    <div class="col-md-6">

        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'surname')->textInput() ?>
        <?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>
        <?= $form->field($model, 'countryId')->dropDownList(Geo::getCountriesMap()) ?>
        <?= $form->field($model, 'telephone')->textInput() ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'passwordConfirm')->passwordInput() ?>
    </div>
    <div class="col-md-6">

        <?= $form->field($model, 'brokerId')->radioList(Brokers::getMap(), ['item' => function ($index, $label, $name, $checked, $value) {
            return Html::label(Html::radio($name, $checked, ['value' => $value,]) . Html::img('/img/broker' . $value . '.png'), null);
        }]) ?>

    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'confirm')->checkbox([
            'label' => 'Я согласен с <a href="#">условиями</a>',
        ]) ?>
    </div>
</div>

<?= Html::submitInput('Зарегистрироваться', ['class' => 'btn', 'name' => 'submit']) ?>

<?php ActiveForm::end() ?>
