<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\widgets;


use app\models\RegisterForm as RegisterFormModel;
use yii\base\Widget;

class RegisterForm extends Widget
{
    public function run()
    {
        $model = new RegisterFormModel();
        return $this->render('register_form', [
            'model' => $model,
        ]);
    }
}