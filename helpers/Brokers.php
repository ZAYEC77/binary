<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\helpers;


class Brokers
{
    public static function getMap()
    {
        return [
            1 => 'Broker #1',
            2 => 'Broker #2',
            3 => 'Broker #3',
            4 => 'Broker #4',
        ];
    }
}